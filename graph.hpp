//
// Created by diediaz on 24-04-19.
//

#ifndef BOSS_COLOURING_GRAPH_HPP
#define BOSS_COLOURING_GRAPH_HPP

// C++ program to print DFS traversal from
// a given vertex in a  given graph
#include<iostream>
#include <unordered_map>
#include <map>
#include <vector>
#include <sstream>



// Graph class represents a directed graph
// using adjacency list representation
class graph {
    // Pointer to an array containing
    // adjacency lists
    std::map<size_t, std::unordered_map<size_t, bool>> adj;

    // A recursive function used by DFS
    void dfs_util(size_t v,
                  std::unordered_map<size_t,bool>& visited,
                  std::vector<size_t>& elm_list);
public:
    graph(); // Constructor

    // function to add an edge to graph
    void add_edge(size_t v, size_t w);

    // DFS traversal of the vertices
    // reachable from v
    std::vector<size_t> dfs_order();

    std::string dimacs_format();
};




#endif //BOSS_COLOURING_GRAPH_HPP
