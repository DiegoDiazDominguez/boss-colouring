extern "C" {
#include <zlib.h>
#include <stdio.h>
#include "boss/include/kseq.h"
#include <string.h>
}

// STEP 1: declare the type of file handler and the read() function
KSEQ_INIT(gzFile, gzread)

int main(int argc, char *argv[]){


    FILE *ofp;
    ofp = fopen("reads_no_Ns.fastq", "w+");

    gzFile fp;  
    kseq_t *seq;  
    int l;  
    if (argc == 1) {  
        fprintf(stderr, "Usage: %s <in.seq>\n", argv[0]);  
        return 1;  
    }

    fp = gzopen(argv[1], "r"); // STEP 2: open the file handler  
    seq = kseq_init(fp); // STEP 3: initialize seq  
    while ((l = kseq_read(seq)) >= 0) { // STEP 4: read sequence  

	    if(strchr(seq->seq.s,'N')==nullptr && strlen(seq->seq.s)==100){
		    fprintf(ofp, "@%s %s\n", seq->name.s, seq->comment.s);
		    fprintf(ofp, "%s\n", seq->seq.s);
		    fprintf(ofp, "+\n");
		    fprintf(ofp, "%s\n",seq->qual.s);
	    }
    }

    fclose(ofp);
    kseq_destroy(seq); // STEP 5: destroy seq
    gzclose(fp); // STEP 6: close the file handler  
    return 0;  
}  


