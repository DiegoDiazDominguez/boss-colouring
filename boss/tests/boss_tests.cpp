//
// Created by Diego Diaz on 2018-12-23.
//
#include "gtest/gtest.h"
#include <boss.hpp>
#include <sdsl/sdsl_concepts.hpp>

TEST(BOSSTests, build){
    std::string fq_path = "/home/ddiaz/omnitigs_assembly_project/hoboss_data/error_free_data/ecoli_15x_f.fq";
    size_t k=145;
    dbg_boss dbg(fq_path, k);
    std::cout<<"size of the edges:"<<std::endl;
    std::cout<<sdsl::size_in_mega_bytes(dbg.edge_bwt)<<std::endl;
}

TEST(BOSSTests, outgoing){

}

TEST(BOSSTests, outdegree){

}

TEST(BOSSTests, indegree){

}

TEST(BOSSTests, incomming){

}

TEST(BOSSTests, getkmer){

}

TEST(BOSSTests, backward_search){
}

TEST(BOSSTests, reverse_compolement){
}

int main(int argc, char **argv){

    if(argc<3){
        std::cout<<"missing arguments"<<std::endl;
        exit(1);
    }
    ::testing::InitGoogleTest(&argc, argv);
    int ret = RUN_ALL_TESTS();
    return ret;
}

