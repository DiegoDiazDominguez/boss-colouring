//
// Created by Diego Diaz on 3/19/18.
//
#ifndef BOSS_HPP
#define BOSS_HPP

#include <iostream>
#include <fstream>
#include <bitset>

#include <sdsl/sdsl_concepts.hpp>
#include <sdsl/int_vector.hpp>
#include <sdsl/construct.hpp>
#include <sdsl/construct_lcp.hpp>
#include <sdsl/construct_sa.hpp>
#include <sdsl/construct_bwt.hpp>
#include <sdsl/wt_algorithm.hpp>

#include "f_array_t.hpp"
#include "fastx_parser.hpp"
#include "dna_alphabet.hpp"

class dbg_boss{

public:
    typedef uint64_t                                    size_type;
    typedef std::pair<size_type, size_type>             range_t;
    typedef std::pair<size_type, bool>                  degree_t;
    typedef std::vector<uint8_t>                        label_t;
    struct bs_t{
        size_type r_start;
        size_type r_end;
        size_type mm;
        bs_t(size_type start, size_type end, size_type mis): r_start(start),
                                                             r_end(end),
                                                             mm(mis){}
    };


private:
    typedef rrr_vector<63>                              comp_bv_t;
    typedef typename sdsl::wt_huff<>                    edges_t;
    typedef typename rrr_vector<63>::rank_0_type        rank_0_t;
    typedef typename rrr_vector<63>::select_0_type      select_0_t;
    typedef typename rrr_vector<63>::select_1_type      select_1_t;

    struct bs_step_t{
        size_type r_start;
        size_type r_end;
        size_type mismatches;
        size_type last_index;
    };

    size_type                                           m_n_solid_nodes;
    size_t                                              m_k;
    f_array_t                                           m_f_array;
    edges_t                                             m_edge_bwt;
    comp_bv_t                                           m_node_marks;
    comp_bv_t                                           m_solid_nodes;
    rank_0_t                                            m_node_marks_rs;
    select_0_t                                          m_node_marks_ss;
    select_1_t                                          m_solid_nodes_ss;
    const uint8_t                                       bit_clear=254;

    //temporal variables for interval_symbols function
    mutable std::vector<edges_t::value_type> cs;
    mutable std::vector<edges_t::size_type> c_i;
    mutable std::vector<edges_t::size_type> c_j;

public:
    const size_t&       k                 =             m_k;
    const size_type&    n_solid_nodes     =             m_n_solid_nodes;
    const f_array_t&    f_array           =             m_f_array;
    const edges_t&      edge_bwt          =             m_edge_bwt;
    const comp_bv_t&    node_marks        =             m_node_marks;
    const rank_0_t&     node_marks_rs     =             m_node_marks_rs;
    const select_0_t&   node_marks_ss     =             m_node_marks_ss;
    const comp_bv_t&    solid_nodes       =             m_solid_nodes;
    const select_1_t&   solid_nodes_ss    =             m_solid_nodes_ss;

public:
    //constructors
    dbg_boss();
    dbg_boss(std::string& input_file, size_t K);

    //navigational functions
    size_type outgoing(size_type v, uint8_t value, bool is_symbol=false) const;
    size_type incomming(size_type v, size_t rank) const;
    degree_t d_outdegree(size_type v) const;
    degree_t d_indegree(size_type v) const;
    uint8_t indegree(size_type v) const; //regular indegree
    uint8_t outdegree(size_type v) const; //regular outdegree

    size_type rev_comp(size_type v) const;
    range_t get_edges(size_type v) const;
    label_t node_label(size_type v) const;
    std::string node2string(size_type v, bool rev=true) const;
    size_type string2node(std::string string) const;
    size_type string2node(const char * string, size_t size) const;
    std::vector<bs_t> backward_search(label_t &query, uint8_t mismatches) const;

    size_type tot_edges() const;
    size_type tot_nodes() const;

    //storage functions
    size_type serialize(std::ostream& out, structure_tree_node* v, std::string name) const;
    void load(std::istream&);

private:
    void build_SA_BWT_LCP(cache_config &config);
    void build_edge_BWT(cache_config &config, size_t K);
    void build_KLCP(cache_config &config, size_t K);
};

inline dbg_boss::range_t dbg_boss::get_edges(size_type v) const {
    size_t start, end;
    if(v==0){
        start=0;
    }else {
        start = m_node_marks_ss.select(v) + 1;
    }
    end = m_node_marks_ss.select(v+1);
    return std::make_pair(start, end);
}

inline dbg_boss::degree_t dbg_boss::d_outdegree(size_type v) const {
    range_t range;
    bool has_dollar;
    range = get_edges(v);
    has_dollar = (m_edge_bwt[range.first]>>1U)==1;
    return std::make_pair(range.second-range.first+1, has_dollar);
}

inline dbg_boss::size_type dbg_boss::incomming(dbg_boss::size_type v, size_t rank) const{
    assert(rank>0);
    if(v==0) return 0;

    uint8_t symbol = m_f_array[v];
    size_type bucket_rank = v-m_f_array.C[symbol]+1;

    uint8_t bwt_symbol = symbol <<1U;
    uint8_t m_bwt_symbol = bwt_symbol | 1U;
    size_type bwt_pos = m_edge_bwt.select(bucket_rank, bwt_symbol);

    size_type next_bwt_pos;
    if((m_f_array.C[symbol+1] - m_f_array.C[symbol])==bucket_rank){
        next_bwt_pos = m_edge_bwt.size();
    }else{
        next_bwt_pos = m_edge_bwt.select(bucket_rank+1, bwt_symbol);
    }

    size_type first_incoming = m_node_marks_rs.rank(bwt_pos);

    if(m_solid_nodes[first_incoming]){
        if(rank==1) return first_incoming;
        rank--;
    }

    size_type marked_before = m_edge_bwt.rank(bwt_pos, m_bwt_symbol);
    assert(rank<=(m_edge_bwt.rank(next_bwt_pos, m_bwt_symbol) - marked_before));
    return m_node_marks_rs.rank(m_edge_bwt.select(marked_before+rank, m_bwt_symbol));
}

inline dbg_boss::degree_t dbg_boss::d_indegree(dbg_boss::size_type v) const {

    uint8_t symbol = m_f_array[v];
    size_type symbol_rank = v - m_f_array.C[symbol]+1;
    size_type symbol_freq = m_f_array.symbol_freq(symbol);

    symbol = symbol<<1U;
    uint8_t symbol_marked = symbol | 1U;

    size_type edge_pos = m_edge_bwt.select(symbol_rank, symbol);
    size_type next_edge_pos;
    if(symbol_rank+1>symbol_freq){
        next_edge_pos = m_edge_bwt.size();
    }else{
        next_edge_pos = m_edge_bwt.select(symbol_rank+1, symbol);
    }

    size_type marked_bf = m_edge_bwt.rank(edge_pos, symbol_marked);
    size_type n_marked = m_edge_bwt.rank(next_edge_pos, symbol_marked)-marked_bf;

    return {1+n_marked, !m_solid_nodes[m_node_marks_rs(edge_pos)]};
}

inline dbg_boss::size_type dbg_boss::outgoing(dbg_boss::size_type v, uint8_t value, bool is_symbol) const {

    range_t edges = get_edges(v);

    if(!is_symbol){//value is treated as rank within the edges

        if((m_edge_bwt[edges.first]>>1U)==1) value++;//in case the first symbol is dollar
        assert(value>0 && value<=(edges.second-edges.first+1));

        auto inv_select = m_edge_bwt.inverse_select(edges.first+value-1);
        uint8_t symbol = inv_select.second;
        size_type rank = inv_select.first;

        if(!(symbol & 1UL)){ //unmarked symbol
            return m_f_array.C[(symbol>>1U)] + rank;
        }else{
            return m_f_array.C[(symbol>>1U)] +
                   m_edge_bwt.rank(edges.first+value-1, (symbol & bit_clear)) - 1;
        }

    }else{//value is treated as a symbol

        assert(value<dna_alphabet::sigma);
        size_type n_symbols=0;
        m_edge_bwt.interval_symbols(edges.first, edges.second+1, n_symbols, cs, c_i, c_j);

        for(size_t i=0;i<n_symbols;i++){
            if((cs[i]>>1U) == value){
                return m_f_array.C[cs[i]>>1U] +
                       m_edge_bwt.rank(edges.first, (cs[i] & bit_clear)) -
                       (cs[i] & 1U);
            }
        }

        return 0;
    }
}//t is the rank inside the range [1..\sigma]

inline dbg_boss::size_type dbg_boss::tot_edges() const {
    return m_edge_bwt.size();
}

inline dbg_boss::size_type dbg_boss::tot_nodes() const {
    return m_f_array.C[dna_alphabet::sigma];
}

inline uint8_t dbg_boss::outdegree(dbg_boss::size_type v) const {
    range_t range = get_edges(v);
    return range.second-range.first+1;
}

inline uint8_t dbg_boss::indegree(dbg_boss::size_type v) const {

    uint8_t symbol = m_f_array[v];
    size_type symbol_freq = m_f_array.symbol_freq(symbol);
    size_type symbol_rank = v - m_f_array.C[symbol]+1;

    symbol = symbol<<1U;
    uint8_t symbol_marked = symbol | 1U;

    size_type edge_pos = m_edge_bwt.select(symbol_rank, symbol);
    size_type next_edge_pos;

    //assert(m_f_array.C[(symbol>>1U)+1]-m_f_array.C[(symbol>>1U)]==m_edge_bwt.rank(m_edge_bwt.size(), symbol));
    if(symbol_rank+1>symbol_freq){
        next_edge_pos = m_edge_bwt.size();
    }else{
        next_edge_pos = m_edge_bwt.select(symbol_rank+1, symbol);
    }

    size_type marked_bf = m_edge_bwt.rank(edge_pos, symbol_marked);
    size_type n_marked = m_edge_bwt.rank(next_edge_pos, symbol_marked)-marked_bf;

    return 1+n_marked;
}

#endif //BOSS_HPP
