//
// Created by Diego Diaz on 3/19/18.
//

#ifndef BOSS_FASTX_PARSER_HPP
#define BOSS_FASTX_PARSER_HPP

extern "C"{
#include "kseq.h"
#include <zlib.h>
}

#include <iostream>
#include <fstream>
#include <algorithm>
#include <sdsl/sd_vector.hpp>
#include "dna_alphabet.hpp"

class fastx_parser {
    KSEQ_INIT(gzFile, gzread);
public:
    static int preproc_reads(std::string &input_file, sdsl::cache_config& config, size_t kmer_size);
};


#endif //BOSS_PARSEDNAFILES_HPP
