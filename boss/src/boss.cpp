//
// Created by Diego Diaz on 3/19/18.
//

#include <boss.hpp>

dbg_boss::dbg_boss(std::string &input_file, size_t K): m_n_solid_nodes(0),
                                                       m_k(K){

    cache_config config;
    fastx_parser::preproc_reads(input_file, config, 1);
    build_SA_BWT_LCP(config);
    build_KLCP(config, K - 1);
    build_edge_BWT(config, K - 1);

    load_from_file(m_node_marks, cache_file_name("node_marks", config));
    m_node_marks_ss.set_vector(&m_node_marks);
    m_node_marks_rs.set_vector(&m_node_marks);

    load_from_file(m_solid_nodes, cache_file_name("solid_nodes", config));
    m_solid_nodes_ss.set_vector(&m_solid_nodes);

    //delete temporal files
    sdsl::util::delete_all_files(config.file_map);

    cs.resize(2*dna_alphabet::sigma);
    c_i.resize(2*dna_alphabet::sigma);
    c_j.resize(2*dna_alphabet::sigma);
}

void dbg_boss::build_SA_BWT_LCP(cache_config &config){
    // construct SA
    construct_sa<8>(config);
    register_cache_file(conf::KEY_SA, config);

    // construct BWT
    construct_bwt<8>(config);
    register_cache_file(conf::KEY_BWT, config);

    // construct LCP
    construct_lcp_semi_extern_PHI(config);
    register_cache_file(conf::KEY_LCP, config);
}

dbg_boss::size_type dbg_boss::serialize(std::ostream &out, structure_tree_node *v, std::string name) const{

    structure_tree_node* child = structure_tree::add_child(v, name, util::class_name(*this));
    size_t written_bytes = 0;
    written_bytes += m_f_array.serialize(out,child, "f_array");
    written_bytes += m_edge_bwt.serialize(out, child, "edge_bwt");
    written_bytes += m_node_marks.serialize(out, child, "node_marks");
    written_bytes += m_node_marks_rs.serialize(out, child, "node_marks_rs");
    written_bytes += m_node_marks_ss.serialize(out, child, "node_marks_ss");
    written_bytes += m_solid_nodes.serialize(out, child, "m_solid_nodes");
    written_bytes += m_solid_nodes_ss.serialize(out, child, "m_solid_nodes_ss");
    written_bytes += write_member(m_k, out, child, "k");
    written_bytes += write_member(m_n_solid_nodes, out, child, "m_n_solid_nodes");

    structure_tree::add_size(child, written_bytes);
    return written_bytes;
}

void dbg_boss::load(std::istream& in){
    m_f_array.load(in);
    m_edge_bwt.load(in);
    m_node_marks.load(in);
    m_node_marks_rs.load(in, &m_node_marks);
    m_node_marks_ss.load(in, &m_node_marks);
    m_solid_nodes.load(in);
    m_solid_nodes_ss.load(in, &m_solid_nodes);
    read_member(m_k, in);
    read_member(m_n_solid_nodes, in);
}

dbg_boss::dbg_boss(): m_n_solid_nodes(0),
                      m_k(0){
    cs.resize(2*dna_alphabet::sigma);
    c_i.resize(2*dna_alphabet::sigma);
    c_j.resize(2*dna_alphabet::sigma);
}


void dbg_boss::build_KLCP(cache_config &config, size_t K) {

    //LOG(INFO)<<"Building the LCP of the kmers";

    size_t n_dollars;
    int_vector_buffer<> lcp(cache_file_name(conf::KEY_LCP, config));
    int_vector_buffer<> klcp(cache_file_name("klcp", config), std::ios::out, 1000000);

    int_vector_buffer<> sa(cache_file_name(conf::KEY_SA, config));
    sdsl::sd_vector<> dollar_bv;
    load_from_file(dollar_bv, cache_file_name("sp_dollar_bv", config));
    sdsl::sd_vector<>::rank_1_type dr(&dollar_bv);
    sdsl::sd_vector<>::select_1_type ds(&dollar_bv);
    n_dollars = dr.rank(dollar_bv.size());

    klcp[0] = 0;
    //dummy node
    for(size_t i=1;i<=n_dollars;i++){
        klcp[i] = K;
    }

    for(size_t i=(n_dollars+1);i<lcp.size();i++){
        //modify the LCP
        if(lcp[i]>K || lcp[i]>=(ds.select(dr.rank(sa[i]+1)+1)-sa[i]+1)){
            klcp[i]=K;
        }else{
            klcp[i] = lcp[i];
        }
    }

    lcp.close();
    klcp.close();
    register_cache_file("klcp", config);
}

void dbg_boss::build_edge_BWT(cache_config &config, size_t K) {

    bool flag_symbols;
    std::bitset<6> kmer_symbols;
    std::bitset<6> flagged_symbols=false;
    bit_vector plain_node_marks;
    int_vector_buffer<8> bwt(cache_file_name(conf::KEY_BWT, config));
    int_vector_buffer<8> ebwt(cache_file_name("ebwt", config), std::ios::out);
    int_vector_buffer<> klcp(cache_file_name("klcp", config));
    size_t bwt_pos=0;

    //this is for marking dollar-prefixed kmers
    sdsl::sd_vector<> dollar_bv;
    bit_vector dollar_marks;
    load_from_file(dollar_bv, cache_file_name("sp_dollar_bv", config));
    sdsl::sd_vector<>::rank_1_type dr(&dollar_bv);
    int_vector_buffer<> sa(cache_file_name(conf::KEY_SA, config));
    util::assign(dollar_marks, int_vector<1>(bwt.size(), 0));
    size_t dpos=0;
    //

    //TODO just testing
    //int_vector_buffer<8> text(cache_file_name(conf::KEY_TEXT, config));
    //size_t kpos=0, epos=0;
    //

    util::assign(plain_node_marks, int_vector<1>(bwt.size(), 1));

    //TODO just testing
    /*
    //print the suffix array
    for(size_t i=0;i<text.size();i++){
        for(size_t j=sa[i];j<text.size();j++){
            std::cout<<f_array_t::comp2char[text[j]];
        }
        std::cout<<""<<std::endl;
    }*/
    //

    for(size_t i=1;i<=klcp.size();i++){

        //TODO just checking
        //bool first=true;

        if(i==klcp.size() || klcp[i]<K){

            //this is for marking solid ndoes
            if(dr.rank(std::min(sa[i-1]+K, sa.size()))-dr.rank(sa[i-1])==0){
                dollar_marks[dpos]=true;
                m_n_solid_nodes++;
            }

            //store the symbols of the previous kmer
            for(size_t j=1;j<kmer_symbols.size();j++){

                if(kmer_symbols[j]){

                    ebwt[bwt_pos]= (j<<1U) | flagged_symbols[j];
                    bwt_pos++;

                    //TODO just checking
                    /*std::cout<<epos<<"\t"<<(ebwt[bwt_pos-1] & 1UL)<<f_array_t::comp2char[j]<<"\t";
                    epos++;
                    if(first) {
                        bool dollar_start = false;
                        for (size_t i1 = 0; i1 < K; i1++) {

                            if ( (sa[i-1]+i1)>=text.size() || (text[sa[i - 1] + i1]) == 1) {
                                dollar_start = true;
                            }

                            if(!dollar_start){
                                std::cout << f_array_t::comp2char[text[sa[i - 1] + i1]];
                            }else{
                                std::cout << "$";
                            }
                        }
                        std::cout<<"\t"<<dollar_marks[dpos]<<"\t"<<kpos;
                        kpos++;
                    }
                    first=false;
                    std::cout<<""<<std::endl;
                     */
                    //
                }
            }
            plain_node_marks[bwt_pos-1] = false;
            dpos++;
            if(i==klcp.size()) break;

            //setup for the next Kmer;
            flag_symbols = klcp[i] == (K - 1);
            if (!flag_symbols) {
                flagged_symbols.reset();
            } else {
                flagged_symbols |= kmer_symbols;
            }
            kmer_symbols.reset();
        }
        kmer_symbols.set(bwt[i]);
    }

    plain_node_marks.resize(bwt_pos);

    ebwt.close();
    klcp.close();

    sdsl::wt_huff<> tmp_wt;
    sdsl::construct(tmp_wt, cache_file_name("ebwt", config));
    m_edge_bwt.swap(tmp_wt);

    int_vector<64> tmp_C;
    tmp_C.resize(dna_alphabet::sigma+1);

    //compute the rank of every symbol
    tmp_C[0]=0;// # symbol
    tmp_C[1] =1; // $ symbol
    for(uint8_t i=2;i<dna_alphabet::sigma;i++) tmp_C[i] = m_edge_bwt.rank(m_edge_bwt.size(),(i<<1U));

    //compute the accumulative values
    /*for(size_t i=0;i<=f_array_t::sigma;i++){
        std::cout<<tmp_C[i]<<std::endl;
    }
    std::cout<<""<<std::endl;*/

    size_type tmp1, tmp2;
    tmp2 = 0;
    for(uint8_t i=1;i<=dna_alphabet::sigma;i++){
        tmp1 = tmp_C[i];
        tmp_C[i] = tmp_C[i-1] + tmp2;
        tmp2 = tmp1;
    }

    /*for(size_t i=0;i<=f_array_t::sigma;i++){
        std::cout<<tmp_C[i]<<std::endl;
    }*/

    f_array_t tmp_alph(tmp_C);
    m_f_array.swap(tmp_alph);

    //std::cout<<m_f_array.C[6]<<std::endl;

    //this is for marking solid nodes
    dollar_marks.resize(dpos);
    sdsl::rrr_vector<63> rrr_dollar_marks(dollar_marks);
    store_to_file(rrr_dollar_marks, cache_file_name("solid_nodes", config));
    //

    sdsl::rrr_vector<63> tmp_node_marks(plain_node_marks);
    store_to_file(tmp_node_marks, cache_file_name("node_marks", config));

    register_cache_file("solid_nodes", config);
    register_cache_file("node_marks", config);
    register_cache_file("ebwt", config);
}

dbg_boss::size_type dbg_boss::rev_comp(dbg_boss::size_type v) const {

    assert(m_solid_nodes[v]);

    label_t label = node_label(v);
    label_t rev_comp_label(m_k-1);

    for(size_t i=0,j=label.size()-1;i<label.size();i++,j--) rev_comp_label[i] = dna_alphabet::comp2rev[label[j]];

    //backward search return the range in the bwt not the label id!!
    std::vector<bs_t> bs_res = backward_search(rev_comp_label, 0);

    assert(!bs_res.empty());
    return bs_res[0].r_start;
}

dbg_boss::label_t dbg_boss::node_label(dbg_boss::size_type v) const {

    label_t label(m_k-1);

    for(size_t i=0;i<m_k-1;i++){
        if(v==0){
            label[i] = 1; //only dollars
            continue;
        }else {
            label[i] = m_f_array[v];
        }
        size_t rank = v - m_f_array.C[label[i]]+1;
        v = m_node_marks_rs.rank(m_edge_bwt.select(rank, label[i]<<1U));
    }
    return label;
}

std::vector<dbg_boss::bs_t> dbg_boss::backward_search(dbg_boss::label_t &query, uint8_t mismatches) const {

    std::stack<bs_step_t> bs_stack;
    std::vector<bs_t>res;
    uint8_t mm, symbol;

    size_type k_start, k_end, n_symbols;

    bs_step_t root = {0, m_edge_bwt.size()-1, 0, query.size()};
    bs_stack.push(root);

    while(!bs_stack.empty()){

        bs_step_t bs_step = bs_stack.top();
        bs_stack.pop();

        if(bs_step.last_index>0) {

            m_edge_bwt.interval_symbols(bs_step.r_start, bs_step.r_end+1,
                                        n_symbols, cs, c_i, c_j);

            for(size_t i=0;i<n_symbols;i++){

                symbol =  cs[i]>>1U;
                if(symbol==1 || (cs[i] & 1U)) continue;

                mm = bs_step.mismatches + (symbol != query[bs_step.last_index - 1]);

                if (mm <= mismatches) {
                    k_start = m_f_array.C[symbol] + c_i[i];
                    k_end = m_f_array.C[symbol] + c_j[i];

                    bs_stack.push({m_node_marks_ss.select(k_start) + 1,
                                   m_node_marks_ss.select(k_end), mm,
                                   bs_step.last_index-1});
                }
            }
        }else {
            res.emplace_back(m_node_marks_rs(bs_step.r_start),
                             m_node_marks_rs(bs_step.r_end),
                             bs_step.mismatches);
        }
    }

    return res;
}


std::string dbg_boss::node2string(dbg_boss::size_type v, bool rev) const {

    label_t label = node_label(v);
    std::string label_str;
    for (unsigned char l : label) label_str.push_back(dna_alphabet::comp2char[l]);
    if(rev) std::reverse(label_str.begin(), label_str.end());
    return label_str;
}

dbg_boss::size_type dbg_boss::string2node(std::string string) const {

    assert(string.size()==(m_k-1));
    label_t label;
    std::copy(string.begin(), string.end(), std::back_inserter(label));
    std::reverse(label.begin(), label.end());
    for(unsigned char & i : label) i = dna_alphabet::char2comp[i];
    std::vector<bs_t> bs = backward_search(label,0);
    if(!bs.empty()) return bs[0].r_start;
    return 0;
}

dbg_boss::size_type dbg_boss::string2node(const char * string, size_t size) const {

    assert(size==(m_k-1));
    label_t label;
    std::copy(string, string+size, std::back_inserter(label));
    std::reverse(label.begin(), label.end());
    for(unsigned char & i : label) i = dna_alphabet::char2comp[i];
    std::vector<bs_t> bs = backward_search(label,0);
    if(!bs.empty()) return bs[0].r_start;
    return 0;
}
