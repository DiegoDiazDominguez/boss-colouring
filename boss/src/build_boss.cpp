//
// Created by diediaz on 04-04-19.
//
#include <boss.hpp>

int main(){

    std::string file = "/home/ddiaz/omnitigs_assembly_project/hoboss_data/error_free_data/ecoli_5x_f.fq";
    dbg_boss dbg_index(file, 30);

    std::cout<<dbg_index.outgoing(335, 1)<<std::endl;
    std::cout<<dbg_index.outgoing(337, 1)<<std::endl;
    std::cout<<dbg_index.outgoing(339, 1)<<std::endl;
    std::cout<<dbg_index.outgoing(340, 1)<<std::endl;

    dbg_boss::degree_t outd = dbg_index.d_outdegree(338);
    std::cout<<"d_outdegree :"<<outd.first<<" "<<outd.second<<" "<<int(dbg_index.outdegree(338))<<std::endl;
    dbg_boss::degree_t ind = dbg_index.d_indegree(320);
    std::cout<<"d_indegree :"<<ind.first<<" "<<ind.second<<" "<<int(dbg_index.indegree(320))<<std::endl;

    /*std::cout<<dbg_index.outgoing(336, 5, true)<<std::endl;
    std::cout<<dbg_index.incomming(340, 1)<<std::endl;
    std::cout<<dbg_index.incomming(340, 3)<<std::endl;*/

    /*dbg_boss::label_t query = {5,5,3,4};
    auto bs = dbg_index.backward_search(query, 1);
    for(size_t i=0;i<bs.size();i++){
        std::cout<<bs[i].r_start<<" "<<bs[i].r_end<<" "<<bs[i].mm<<std::endl;
    }*/

    size_t n_solid=0;
    for(size_t i=0;i<dbg_index.solid_nodes.size();i++){
        if(dbg_index.solid_nodes[i]){
            if(i!= dbg_index.string2node(dbg_index.node2string(i).c_str(), dbg_index.k-1) ){
                std::cout<<"problema"<<std::endl;
            }
        }
    }
    std::cout<<n_solid<<" "<<dbg_index.n_solid_nodes<<std::endl;

    /*for(dbg_boss::size_type i=1;i<=dbg_index.n_solid_nodes;i++){
        dbg_boss::size_type tmp_node = dbg_index.solid_nodes_ss(i);
        std::cout<<dbg_index.node2string(tmp_node, true)<<std::endl;
    }
    std::cout<<dbg_index.node2string(340)<<std::endl;
    std::cout<<dbg_index.node2string(dbg_index.rev_comp(332), true)<<std::endl;
    std::cout<<dbg_index.node2string(332)<<std::endl;
     */

}

