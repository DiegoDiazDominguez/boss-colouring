cmake_minimum_required(VERSION 3.5)
project(boss_colouring)

set(CMAKE_CXX_STANDARD 14)

set(PERFORMANCE_FLAGS "-msse4.2 -O3 -funroll-loops -fomit-frame-pointer -ffast-math")

set(CMAKE_RUNTIME_OUTPUT_DIRECTORY ${CMAKE_SOURCE_DIR}/bin)

set(CMAKE_CXX_FLAGS  "${CMAKE_CXX_FLAGS} ${PERFORMANCE_FLAGS}" )

add_subdirectory(boss)

add_executable(remove_N_reads remove_N_reads.cpp)
add_executable(filter_unitig_reads filter_unitig_reads.cpp)
add_executable(build_boss_index build_boss_index.cpp)
add_executable(mark_reads mark_reads.cpp graph.cpp)

target_link_libraries(remove_N_reads z)
target_link_libraries(mark_reads boss)
target_link_libraries(build_boss_index boss)
target_link_libraries(filter_unitig_reads boss)
