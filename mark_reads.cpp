//
// Created by diediaz on 18-12-18.
//

#include <iostream>
#include <boss.hpp>
#include <sdsl/suffix_arrays.hpp>
#include "graph.hpp"

KSEQ_INIT(gzFile, gzread)

using namespace std;
using namespace sdsl;

struct union_find{

   std::vector<size_t> rep;
   std::vector<size_t> size;

   union_find(std::vector<std::pair<size_t, size_t>>& bipartite, size_t n_reads){

      rep.resize(n_reads);
      size.resize(n_reads);
      std::iota(std::begin(rep), std::end(rep), 0);
      std::fill(size.begin(), size.end(),1);

       std::pair<size_t, size_t> prev_edge = bipartite[0];
       for(size_t i=1;i<bipartite.size();i++){
           if(bipartite[i].second == prev_edge.second){

               t_union(prev_edge.first, bipartite[i].first);
           }
           prev_edge = bipartite[i];
       }
   }

   std::vector<std::vector<size_t>> get_components(){

       std::vector<std::pair<size_t, size_t>> elms;
       std::vector<std::vector<size_t>> res;
       std::vector<size_t> tmp;

       for(size_t i=0;i<rep.size();i++){
           elms.emplace_back(i, rep[i]);
       }

       std::sort(elms.begin(), elms.end(), [](const std::pair<size_t, size_t> &x,
                                              const std::pair<size_t, size_t> &y){
           return x.second < y.second;
       });

       tmp.push_back(elms[0].first);
       std::pair<size_t, size_t> prev_elm;
       prev_elm = elms[0];
       for(size_t i=1;i<elms.size();i++){

           if(elms[i].second == prev_elm.second){
               tmp.push_back(elms[i].first);
           }else{
               res.push_back(tmp);
               tmp.clear();
               tmp.push_back(elms[i].first);
           }
           prev_elm = elms[i];
       }
       res.push_back(tmp);
       return res;
   };

   std::map<size_t, graph> get_comp_subgraphs(
           std::vector<std::pair<size_t, size_t>> &bipartite){

       std::map<size_t, graph> c_graph;
       std::vector<size_t> c_nodes;

       size_t component;

       c_nodes.push_back(bipartite[0].first);
       for(size_t i=1;i<bipartite.size();i++){

           if(bipartite[i].second == bipartite[i-1].second){
               c_nodes.push_back(bipartite[i].first);
           }else{

               //get the component
               component = rep[c_nodes[0]];
               //TODO just for testing I am doing it well
               /*for(size_t j=1;j<c_nodes.size();j++){
                   assert(rep[c_nodes[j]]==component);
               }*/

               //get all possible edges
               for(size_t j=0;j<c_nodes.size();j++){
                   for(size_t k=j+1;k<c_nodes.size();k++){

                       c_graph[component].add_edge(c_nodes[j], c_nodes[k]);
                       c_graph[component].add_edge(c_nodes[k], c_nodes[j]);
                   }
               }

               c_nodes.clear();
               c_nodes.push_back(bipartite[i].first);
           }
       }

       return c_graph;
   }

   void t_union(size_t i, size_t j){
       i = t_find(i);
       j = t_find(j);

       if(i==j){
           return;
       }

       if(size[i]<size[j]){
           rep[i] = j;
           size[j] +=size[i];
       }else{
           rep[j] = i;
           size[i] +=size[j];
       }
   }

   size_t t_find(size_t i){
       if(rep[i]==i){
           return i;
       }
       rep[i] = t_find(rep[i]);
       return(rep[i]);
   }
};

const std::string currentDateTime() {
    time_t     now = time(nullptr);
    struct tm  tstruct={};
    char       buf[80];
    tstruct = *localtime(&now);
    strftime(buf, sizeof(buf), "%Y-%m-%d.%X", &tstruct);
    return buf;
}

#define ELOG(msg) \
    std::cout << "[" << currentDateTime() << "]: " << msg << std::endl

int main(int argc, char* argv[]){

    if (argc <4) {
        std::cerr << "Usage: " << argv[0] << " FILTERED_FASTX_FILE BOSS_INDEX OUTPUT_PREFIX" << std::endl;
        return 1;
    }

    std::vector<std::pair<size_t, size_t>> bipartite_graph;
    size_t seqs_pos=0;
    ELOG("Creating the graph of the reads");
    {
        typedef dbg_boss::size_type size_type;
        dbg_boss dbg_index;
        size_type tmp_node;
        std::map<std::pair<size_type, size_t>, bool> links;

        ELOG("Loading BOSS index from file " + std::string(argv[2]));
        load_from_file(dbg_index, argv[2]);
        ELOG("Index loaded");

        //TODO testing
        /*std::string seq = std::string("TTATGCTGAAATAATGTCCAGCGGCAGGATGCATTATCAGAATGTATTTATATTTATTAAATAATAAAAAAAGCCCGTGAATATTCACGGGCTTTATGTAATTTACATTGAATTATTTTTTCTCGGACAGATATTTCACTGTATCAGCAT");
        uint8_t symbol;
        for(size_t j=0;j<2;j++) {

            if(j==1){//compute the reverse complement of current read
                for(int i= 0, k = seq.size()-1; i < k; i++, k--){
                    symbol = seq[i];
                    seq[i] = dna_alphabet::char2rev[seq[k]];
                    seq[k] =  dna_alphabet::char2rev[symbol];
                }
            }

            //compute the position of the first kmer of the read in the dBG
            tmp_node = dbg_index.string2node(seq.c_str(), size_t(dbg_index.k-1));

            //traverse the path in the dBG that spells the sequence of the read
            for (int i = dbg_index.k - 1; i <seq.size(); i++) {
                //std::cout<<dbg_index.node2string(tmp_node)<<std::endl;
                if (dbg_index.outdegree(tmp_node) > 1 || dbg_index.indegree(tmp_node) > 1) {
                    std::cout<<dbg_index.node2string(tmp_node)<<" "<<tmp_node<<" "<<
                    int(dbg_index.outdegree(tmp_node))<<" "<<int(dbg_index.indegree(tmp_node))<<std::endl;
                }

                symbol = dna_alphabet::char2comp[seq[i]];
                tmp_node = dbg_index.outgoing(tmp_node, symbol, true);
            }

            if (dbg_index.outdegree(tmp_node) > 1 || dbg_index.indegree(tmp_node) > 1) {
                std::cout<<dbg_index.node2string(tmp_node)<<" "<<tmp_node<<" "<<
                         int(dbg_index.outdegree(tmp_node))<<" "<<int(dbg_index.indegree(tmp_node))<<std::endl;
            }
        }
        std::map<size_type, bool> test = {{121700,true},
                                          {69449,true},
                                          {305940,true},
                                          {276227,true},
                                          {71619,true},
                                          {12341,true},
                                          {371255,true},
                                          {196651,true},
                                          {167653,true},
                                          {45129,true},
                                          {339202,true},
                                          {131176,true},
                                          {289011,true},
                                          {66438,true},
                                          {111351,true},
                                          {127675,true},
                                          {264614,true},
                                          {168398,true},
                                          {69891,true},
                                          {12745,true},
                                          {104095,true},
                                          {305093,true},
                                          {116218,true},
                                          {177268,true}};*/
        //TODO finish testing

        ELOG("Computing the graph of the reads from file "<<string(argv[1]));
        gzFile fp;
        kseq_t *seq;
        int l;
        std::string file = argv[1];
        fp = gzopen(file.c_str(), "r");
        seq = kseq_init(fp);
        uint8_t symbol;

        while ((l = kseq_read(seq)) >= 0) {

            for(size_t j=0;j<2;j++) {

                if(j==1){//compute the reverse complement of current read
                    for(int i= 0, k = l-1; i < k; i++, k--){
                        symbol = seq->seq.s[i];
                        seq->seq.s[i] = dna_alphabet::char2rev[seq->seq.s[k]];
                        seq->seq.s[k] =  dna_alphabet::char2rev[symbol];
                    }
                }

                //compute the position of the first kmer of the read in the dBG
                tmp_node = dbg_index.string2node(seq->seq.s, size_t(dbg_index.k-1));

                //traverse the path in the dBG that spells the sequence of the read
                for (int i = dbg_index.k - 1; i < l; i++) {

                    if (dbg_index.outdegree(tmp_node) > 1 || dbg_index.indegree(tmp_node) > 1) {
                        if (links.count({tmp_node, seqs_pos}) == 0) {
                            bipartite_graph.emplace_back(seqs_pos, tmp_node);
                            links[{tmp_node, seqs_pos}] = true;
                        }
                    }

                    symbol = dna_alphabet::char2comp[seq->seq.s[i]];
                    tmp_node = dbg_index.outgoing(tmp_node, symbol, true);
                }

                //check for the last node
                if (dbg_index.outdegree(tmp_node) > 1 || dbg_index.indegree(tmp_node) > 1) {
                    if (links.count({tmp_node, seqs_pos}) == 0) {
                        bipartite_graph.emplace_back(seqs_pos, tmp_node);
                        links[{tmp_node, seqs_pos}] = true;
                    }
                }
            }
            seqs_pos++;
        }

        kseq_destroy(seq);
        gzclose(fp);
    }

    ELOG("Getting the connected components of the graph");
    //sort by kmer ids
    std::sort(bipartite_graph.begin(), bipartite_graph.end(), [](const std::pair<size_t, size_t> &x,
                                                     const std::pair<size_t, size_t> &y){
        return x.second < y.second;
    });

    union_find uf_dt(bipartite_graph, seqs_pos);
    std::map<size_t, graph> components = uf_dt.get_comp_subgraphs(bipartite_graph);
    ELOG(components.size()<<" components were generated");


    ELOG("Generating FASTQ files for every graph component (DFS order)");
    std::vector<std::pair<string, string>> read_seqs;
    gzFile fp;
    kseq_t *seq;
    fp = gzopen(argv[1], "r");
    seq = kseq_init(fp);
    while (kseq_read(seq) >= 0) {
        read_seqs.emplace_back(string(seq->name.s), string(seq->seq.s)+"\n+\n"+string(seq->qual.s));
    }
    kseq_destroy(seq);
    gzclose(fp);

    for(auto& comp_info: components){
        ofstream os_fq;
        os_fq.open(string(argv[3])+"_component_"+to_string(comp_info.first)+".dimacs");
        os_fq << comp_info.second.dimacs_format();

        /*
        ostringstream component_fasta;
        ofstream os_fq;
        std::vector<size_t> ids = comp_info.second.dfs_order();
        os_fq.open(string(argv[3])+"_component_"+to_string(comp_info.first)+".fastq");
        for (auto const& k : ids) {
            component_fasta<<"@"<<read_seqs[k].first<<"\n"<<read_seqs[k].second<<"\n";
        }
        os_fq << component_fasta.str();
        os_fq.close();*/
    }
    return 0;
}