//
// Created by diediaz on 24-04-19.
//

#include "graph.hpp"

void graph::add_edge(size_t v, size_t w){
    adj[v][w]=true; // Add w to v’s list.
}

void graph::dfs_util(const size_t v, std::unordered_map<size_t, bool>& visited, std::vector<size_t>& elm_list) {
    // Mark the current node as visited and
    // print it
    visited[v] = true;
    elm_list.push_back(v);

    // Recur for all the vertices adjacent
    // to this vertex
    for(auto const& v_neighbor : adj[v]){
        if(!visited[v_neighbor.first]){
            dfs_util(v_neighbor.first, visited, elm_list);
        }
    }

    /*std::vector<size_t>::iterator i;
    for (i = adj[v].begin(); i != adj[v].end(); ++i){
        if (!visited[*i]) dfs_util(*i, visited, elm_list);
    }*/
}

// DFS traversal of the vertices reachable from v.
// It uses recursive DFSUtil()
std::vector<size_t> graph::dfs_order() {

    // Mark all the vertices as not visited
    std::unordered_map<size_t, bool> visited;

    /*for (int i = 0; i < adj.size(); i++)
        visited[i] = false;*/

    // Call the recursive helper function
    // to print DFS traversal
    std::vector<size_t> elm_list;
    dfs_util(adj.begin()->first, visited, elm_list);
    return elm_list;
}

graph::graph() {

}

std::string graph::dimacs_format() {

    std::stringstream ss;
    size_t n_edges=0;

    std::map<size_t, size_t> id_mapping;
    size_t i=0;

    for(auto const& node : adj){
        n_edges+=node.second.size();
        id_mapping[node.first] = i;
        i++;
    }

    ss <<"p edge "<<adj.size()<<" "<<n_edges/2<<"\n";

    for(auto const& node : adj){
        for(auto const& neighbor: node.second){
            if(neighbor.first>node.first){
                ss<<"e "<<id_mapping[node.first]<<" "<<id_mapping[neighbor.first]<<"\n";
            }
        }
    }

    return ss.str();
}
