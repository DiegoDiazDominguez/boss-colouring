# Pipeline for preprocessing reads for colouring dBGs

This repository contains a small pipeline to preprocess a set of reads as Travis mentioned in an email:

1. build the uncoloured BOSS for all the reads (whose nodes are the (k - 1)-mers in the reads)
2. filter out the reads that are contained in unitigs, as before
3. build an uncoloured BOSS for the remaining reads
4. mark with a bitstring all the nodes of the new dBG where remaining reads start or end, or that have in- or out-degree more than 1
5. for each read r_i that contains a marked node v_j, output a pair (ri, vj) --- i.e., something like (r1234, v5678).

The pipeline is composed of several C++ scripts; all them connected in a bash file called "boss_colouring_filter.sh".

Prerequisites:

* SDSL library
* Cmake >= 3.7

Installation:

* clone repository
* mkdir build && cd build
* cmake ..
* make

All the files will be placed in the bin folder in the source directory.

Usage:

`$bin/boss_colouring_filter.sh input_file.fastq kmer_size output_prefix`

The output of the pipeline is a file named ${output_prefix}_filtered_edges.txt which contains a list of edges as in 5). 

Some notes:

* The input file must be a FASTA or FASTQ file, but it can't have N symbols. This constraint is due to my BOSS implementation.
* Unititg reads are those reads completely contained within unitigs.
* During the filtering of unitig reads, if a dbg node has an edge labeled with $, then that edge is ignored. 
* The pipeline contains a variation of the BOSS data structure, which can answer some extra queries.
