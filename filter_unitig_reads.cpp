//
// Created by Diego Diaz on 2018-12-17.
//
extern "C"{
#include "kseq.h"
#include <zlib.h>
};

#include <iostream>
#include <boss.hpp>
#include <sdsl/suffix_arrays.hpp>

KSEQ_INIT(gzFile, gzread)

const std::string currentDateTime() {
    time_t     now = time(nullptr);
    struct tm  tstruct={};
    char       buf[80];
    tstruct = *localtime(&now);
    strftime(buf, sizeof(buf), "%Y-%m-%d.%X", &tstruct);
    return buf;
}

#define ELOG(msg) \
    std::cout << "[" << currentDateTime() << "]: " << msg << std::endl


int main(int argc, char* argv[]){

    if (argc < 3) {
        std::cerr << "Usage: " << argv[0] << " FASTQ_FILE BOSS_INDEX OUTPUT_PREFIX" << std::endl;
        return 1;
    }

    ELOG("Filtering reads from unary paths");

    std::string file = argv[1];
    dbg_boss dbg_index;
    ELOG("Loading dBG index from file "<<argv[2]);
    sdsl::load_from_file(dbg_index, argv[2]);

    gzFile fp;
    kseq_t *seq;
    int l;

    fp = gzopen(file.c_str(), "r");
    seq = kseq_init(fp);

    dbg_boss::size_type tmp_node;
    dbg_boss::degree_t ind, outd;

    size_t n_kept=0, tot_seqs=0;
    bool in_unitig;
    uint8_t symbol;

    std::ostringstream filtered_fasta;
    std::ofstream os;
    os.open(std::string(argv[3])+"_filtered.fastq");

    while ((l = kseq_read(seq)) >= 0) {

        in_unitig = true;

        //get the position in the dBG of the first kmer of the read
        tmp_node = dbg_index.string2node(seq->seq.s, dbg_index.k - 1);
        outd = dbg_index.d_outdegree(tmp_node);

        if ((outd.first - outd.second) == 1) {//check if the first node has d_outdegree one

            for (int i = dbg_index.k - 1; i < (l - 1); i++) {
                symbol = dna_alphabet::char2comp[seq->seq.s[i]];
                tmp_node = dbg_index.outgoing(tmp_node, symbol, true);
                outd = dbg_index.d_outdegree(tmp_node);
                ind = dbg_index.d_indegree(tmp_node);

                if ((outd.first - outd.second) > 1 || (ind.first - ind.second) > 1) {
                    in_unitig = false;
                    break;
                }
            }

            if (in_unitig) {//check if the last node has d_indegree more than 1
                symbol = dna_alphabet::char2comp[seq->seq.s[l-1]];
                tmp_node = dbg_index.outgoing(tmp_node, symbol, true);
                ind = dbg_index.d_indegree(tmp_node);
                if (ind.first - ind.second > 1) in_unitig = false;
            }
        } else {
            in_unitig = false;
        }

        if (!in_unitig) {
            filtered_fasta << "@" + std::string(seq->name.s) + "\n";
            filtered_fasta << std::string(seq->seq.s) + "\n+\n";
            filtered_fasta << std::string(seq->qual.s)+"\n";
            n_kept++;
        }
        tot_seqs++;
    }
    kseq_destroy(seq);
    gzclose(fp);

    os << filtered_fasta.str();
    ELOG(n_kept<<" sequences out of "<<tot_seqs<<" were kept");
    os.close();
    return 0;
}
