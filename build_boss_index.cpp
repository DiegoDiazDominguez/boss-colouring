#include <iostream>
#include <boss.hpp>

using namespace std;
using namespace sdsl;

const std::string currentDateTime() {
    time_t     now = time(nullptr);
    struct tm  tstruct={};
    char       buf[80];
    tstruct = *localtime(&now);
    strftime(buf, sizeof(buf), "%Y-%m-%d.%X", &tstruct);
    return buf;
}

#define ELOG(msg) \
    std::cout << "[" << currentDateTime() << "]: " << msg << std::endl

int main(int argc, char* argv[]){

    if (argc < 4) {
        std::cerr << "Usage: " << argv[0] << " FASTQ_FILE K OUTPUT_PREFIX" << std::endl;
        return 1;
    }

    cache_config config(false, ".", "tmp");
    size_t K= size_t(std::stoi(argv[2]));
    std::string input_fastq=argv[1];
    std::string output_prefx = argv[3];

    ELOG("Creating BOSS index from file "<<argv[1]);
    dbg_boss boss_index(input_fastq, K);
    store_to_file(boss_index, output_prefx+".boss");
    return 0;
}