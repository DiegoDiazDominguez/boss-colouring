#!/usr/bin/env bash
EXPECTED_ARGS=3

if [[ $# -ne $EXPECTED_ARGS ]]
then
  echo "Usage: ./program INPUT_FASTQ_FILE KMER_SIZE OUTPUT_PREFIX"
  exit -1
fi

input_fq_file="$1"
kmer_size="$2"
output_prefix="$3"

./build_boss_index ${input_fq_file} ${kmer_size} ${output_prefix}
./filter_unitig_reads ${input_fq_file} ${output_prefix}.boss ${output_prefix}
./build_boss_index ${output_prefix}_filtered.fastq ${kmer_size} ${output_prefix}_filtered
./mark_reads ${output_prefix}_filtered.fastq ${output_prefix}_filtered.boss ${output_prefix}



